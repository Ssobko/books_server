-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.50-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных books
CREATE DATABASE IF NOT EXISTS `books` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `books`;


-- Дамп структуры для таблица books.chats
CREATE TABLE IF NOT EXISTS `chats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) unsigned DEFAULT NULL,
  `mateId` int(11) unsigned DEFAULT NULL,
  `clubId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_chats_users` (`userId`),
  KEY `FK_chats_users_2` (`mateId`),
  KEY `FK_chats_clubs` (`clubId`),
  CONSTRAINT `FK_chats_clubs` FOREIGN KEY (`clubId`) REFERENCES `clubs` (`id`),
  CONSTRAINT `FK_chats_users` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_chats_users_2` FOREIGN KEY (`mateId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы books.chats: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `chats` DISABLE KEYS */;
INSERT INTO `chats` (`id`, `userId`, `mateId`, `clubId`) VALUES
	(2, 8, 6, NULL),
	(5, NULL, NULL, 1);
/*!40000 ALTER TABLE `chats` ENABLE KEYS */;


-- Дамп структуры для таблица books.chat_messages
CREATE TABLE IF NOT EXISTS `chat_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorId` int(10) unsigned NOT NULL,
  `chatId` int(11) NOT NULL,
  `message` text,
  `postedDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_chat_messages_users` (`authorId`),
  KEY `FK_chat_messages_chats` (`chatId`),
  CONSTRAINT `FK_chat_messages_chats` FOREIGN KEY (`chatId`) REFERENCES `chats` (`id`),
  CONSTRAINT `FK_chat_messages_users` FOREIGN KEY (`authorId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы books.chat_messages: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `chat_messages` DISABLE KEYS */;
INSERT INTO `chat_messages` (`id`, `authorId`, `chatId`, `message`, `postedDateTime`) VALUES
	(46, 6, 2, 'Привет, любимая жена!', '2021-06-21 16:45:30'),
	(47, 8, 2, 'Привет, любимый муж! ', '2021-06-21 17:19:37'),
	(54, 6, 5, 'Hey all!', '2021-06-21 23:54:58'),
	(55, 8, 5, 'Hello there', '2021-06-21 23:55:17'),
	(56, 6, 5, '123', '2021-06-22 00:08:14');
/*!40000 ALTER TABLE `chat_messages` ENABLE KEYS */;


-- Дамп структуры для таблица books.clubs
CREATE TABLE IF NOT EXISTS `clubs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(50) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы books.clubs: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `clubs` DISABLE KEYS */;
INSERT INTO `clubs` (`id`, `name`, `date_created`) VALUES
	(1, 'MyClub', '2021-06-04 00:03:54');
/*!40000 ALTER TABLE `clubs` ENABLE KEYS */;


-- Дамп структуры для таблица books.clubs_participants
CREATE TABLE IF NOT EXISTS `clubs_participants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clubId` int(11) NOT NULL,
  `userId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_clubs_participants_clubs` (`clubId`),
  KEY `FK_clubs_participants_users` (`userId`),
  CONSTRAINT `FK_clubs_participants_clubs` FOREIGN KEY (`clubId`) REFERENCES `clubs` (`id`),
  CONSTRAINT `FK_clubs_participants_users` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы books.clubs_participants: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `clubs_participants` DISABLE KEYS */;
INSERT INTO `clubs_participants` (`id`, `clubId`, `userId`) VALUES
	(3, 1, 6),
	(4, 1, 8);
/*!40000 ALTER TABLE `clubs_participants` ENABLE KEYS */;


-- Дамп структуры для таблица books.games
CREATE TABLE IF NOT EXISTS `games` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(11) unsigned NOT NULL,
  `userPoints` int(50) unsigned NOT NULL DEFAULT '0',
  `userRounds` int(1) unsigned NOT NULL DEFAULT '0',
  `opponentId` int(11) unsigned NOT NULL,
  `opponentPoints` int(50) unsigned NOT NULL DEFAULT '0',
  `opponentRounds` int(1) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 - problem was found, 1 - created, 2 - user played round, 3 - opponent played round, 4 - finished',
  `expiredDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_games_users` (`userId`),
  KEY `FK_games_users_2` (`opponentId`),
  KEY `FK_games_games_status` (`status`),
  CONSTRAINT `FK_games_games_status` FOREIGN KEY (`status`) REFERENCES `games_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_games_users` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_games_users_2` FOREIGN KEY (`opponentId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы books.games: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `games` DISABLE KEYS */;
INSERT INTO `games` (`id`, `userId`, `userPoints`, `userRounds`, `opponentId`, `opponentPoints`, `opponentRounds`, `status`, `expiredDate`) VALUES
	(7, 6, 7, 1, 8, 0, 0, 2, '2021-06-18 21:08:30'),
	(8, 8, 0, 0, 6, 7, 1, 3, '2021-06-18 21:08:30');
/*!40000 ALTER TABLE `games` ENABLE KEYS */;


-- Дамп структуры для таблица books.games_status
CREATE TABLE IF NOT EXISTS `games_status` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name_EN` char(50) DEFAULT NULL,
  `name_RU` char(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы books.games_status: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `games_status` DISABLE KEYS */;
INSERT INTO `games_status` (`id`, `name_EN`, `name_RU`) VALUES
	(1, 'User\'s turn', 'Ход игрока'),
	(2, 'Opponent\'s turn', 'Ход соперника'),
	(3, '1', NULL),
	(4, '1', NULL),
	(5, 'Game finished', 'Игра окончена');
/*!40000 ALTER TABLE `games_status` ENABLE KEYS */;


-- Дамп структуры для таблица books.log
CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table` char(50) DEFAULT NULL,
  `row_id` int(11) DEFAULT NULL,
  `column_name` char(50) DEFAULT NULL,
  `value_old` char(255) DEFAULT NULL,
  `value_new` char(255) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы books.log: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;


-- Дамп структуры для таблица books.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `shopItemId` int(11) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы books.orders: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`id`, `userId`, `shopItemId`, `date`, `price`) VALUES
	(1, 6, 5, '2021-05-22 14:40:01', 10),
	(2, 6, 2, '2021-05-22 21:13:52', 10),
	(3, 6, 1, '2021-06-21 21:20:37', 10),
	(4, 6, 4, '2021-06-21 21:20:42', 10),
	(5, 6, 3, '2021-06-21 21:20:44', 10);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;


-- Дамп структуры для таблица books.recipes
CREATE TABLE IF NOT EXISTS `recipes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoryId` int(11) NOT NULL,
  `name` char(50) NOT NULL,
  `description` text,
  `ingredients` char(50) DEFAULT NULL,
  `rarity` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  KEY `FK_recipes_recipes_categories` (`categoryId`),
  CONSTRAINT `FK_recipes_recipes_categories` FOREIGN KEY (`categoryId`) REFERENCES `recipes_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы books.recipes: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `recipes` DISABLE KEYS */;
INSERT INTO `recipes` (`id`, `categoryId`, `name`, `description`, `ingredients`, `rarity`) VALUES
	(1, 1, 'Борщ', 'Отварить говядину', 'Говядина;Лук;Картофель', b'1'),
	(2, 2, 'Жареная картошка', 'Порезать картофель', 'Картофель;Подсолнечное масло;Лук', b'1'),
	(3, 3, 'Роллы из лаваша', 'Взять лаваш', 'Лаваш;Крабовые палочки', b'1'),
	(4, 4, 'Тирамису', 'Бейлиз', NULL, b'1'),
	(5, 5, 'Коктейль "Лонг Айленд"', 'Смешать ХЗ с ХЗ', 'ХЗ; ХЗ', b'1');
/*!40000 ALTER TABLE `recipes` ENABLE KEYS */;


-- Дамп структуры для таблица books.recipes_categories
CREATE TABLE IF NOT EXISTS `recipes_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы books.recipes_categories: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `recipes_categories` DISABLE KEYS */;
INSERT INTO `recipes_categories` (`id`, `name`) VALUES
	(1, 'Первые блюда'),
	(2, 'Вторые блюда'),
	(3, 'Закуски'),
	(4, 'Десерты'),
	(5, 'Напитки');
/*!40000 ALTER TABLE `recipes_categories` ENABLE KEYS */;


-- Дамп структуры для таблица books.shop_items
CREATE TABLE IF NOT EXISTS `shop_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(50) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `category` tinyint(3) unsigned NOT NULL,
  `recipe_category` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_shop_items_shop_items_categories` (`category`),
  KEY `FK_shop_items_recipes_categories` (`recipe_category`),
  CONSTRAINT `FK_shop_items_recipes_categories` FOREIGN KEY (`recipe_category`) REFERENCES `recipes_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_shop_items_shop_items_categories` FOREIGN KEY (`category`) REFERENCES `shop_items_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы books.shop_items: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `shop_items` DISABLE KEYS */;
INSERT INTO `shop_items` (`id`, `name`, `price`, `category`, `recipe_category`) VALUES
	(1, 'Рецепт первого\r\nблюда', 10, 2, 1),
	(2, 'Рецепт второго\r\nблюда', 10, 2, 2),
	(3, 'Рецепт\r\nзакуски', 10, 2, 3),
	(4, 'Рецепт\r\nдесерта', 10, 2, 4),
	(5, 'Рецепт\r\nнапитка', 10, 2, 5);
/*!40000 ALTER TABLE `shop_items` ENABLE KEYS */;


-- Дамп структуры для таблица books.shop_items_categories
CREATE TABLE IF NOT EXISTS `shop_items_categories` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы books.shop_items_categories: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `shop_items_categories` DISABLE KEYS */;
INSERT INTO `shop_items_categories` (`id`, `name`) VALUES
	(1, 'Купить'),
	(2, 'Обменять'),
	(3, 'Награды');
/*!40000 ALTER TABLE `shop_items_categories` ENABLE KEYS */;


-- Дамп структуры для таблица books.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `active` int(1) unsigned NOT NULL DEFAULT '1',
  `internalId` char(50) NOT NULL,
  `nickname` char(50) DEFAULT NULL,
  `email` char(50) DEFAULT NULL,
  `gold` int(50) unsigned NOT NULL DEFAULT '0',
  `maxGold` int(255) unsigned NOT NULL DEFAULT '0',
  `rating` int(10) unsigned NOT NULL DEFAULT '1000',
  `maxSeasonRating` int(10) unsigned NOT NULL DEFAULT '1000',
  `maxRating` int(10) unsigned NOT NULL DEFAULT '1000',
  `lastLogin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `winsCount` int(50) unsigned NOT NULL DEFAULT '0',
  `gamesCount` int(50) unsigned NOT NULL DEFAULT '0',
  `source` char(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы books.users: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `active`, `internalId`, `nickname`, `email`, `gold`, `maxGold`, `rating`, `maxSeasonRating`, `maxRating`, `lastLogin`, `winsCount`, `gamesCount`, `source`) VALUES
	(6, 1, '114538180346311857839', 'Mr Dungeon Master', 'i.dungeon.d@gmail.com', 718, 0, 1000, 1000, 1000, '2021-03-03 02:57:52', 0, 0, 'Google'),
	(7, 1, '468016034', 'Dungeon Master', '', 0, 0, 1000, 1000, 1000, '2021-03-03 02:59:07', 0, 0, 'VK'),
	(8, 1, '115503996751837162491', 'Дина Фаразутдинова', '', 0, 0, 1000, 1000, 1000, '2021-03-03 19:06:52', 0, 0, 'Google');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Дамп структуры для таблица books.users_recipes
CREATE TABLE IF NOT EXISTS `users_recipes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) unsigned NOT NULL,
  `recipeId` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_users_recipes_recipes` (`recipeId`),
  KEY `FK_users_recipes_users` (`userId`),
  CONSTRAINT `FK_users_recipes_recipes` FOREIGN KEY (`recipeId`) REFERENCES `recipes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_users_recipes_users` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы books.users_recipes: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `users_recipes` DISABLE KEYS */;
INSERT INTO `users_recipes` (`id`, `userId`, `recipeId`, `date`) VALUES
	(1, 6, 5, '2021-05-22 14:40:01'),
	(2, 6, 2, '2021-05-22 21:13:52'),
	(3, 6, 1, '2021-06-21 21:20:37'),
	(4, 6, 4, '2021-06-21 21:20:42'),
	(5, 6, 3, '2021-06-21 21:20:44');
/*!40000 ALTER TABLE `users_recipes` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

<?php
	header("Content-type: application/json; charset=utf-8");

	file_put_contents('errors.log', "start INSERT ".date("d.m.Y H:i:s").PHP_EOL,FILE_APPEND);
	if(isset($_POST)) {
		file_put_contents('errors.log', "request: ".var_export($_POST,true).PHP_EOL,FILE_APPEND);
		require '../db_connect.php';
		$db = new DB_CONNECT();

		$gameData = array("id" => 0);
		if(isset($_POST['userId']) && $_POST['userId'] > 0) {
			$userId = $_POST['userId'];
			settype($userId, "int");

			$user = $db->selectUserByID($userId);
			file_put_contents('errors.log', "query result: ".var_export($user, true).PHP_EOL,FILE_APPEND);

			// $winsPercent = ($user['gamesCount'] == 0) ? 0 : round($user['winsCount'] / $user['gamesCount'] * 100);

			$opponents = $db->select(
				"users.id, users.nickname",
				"users",
				"(users.id != ".$userId.")". 
				"AND (active = 1) ". 
				"AND (rating >= ".$user['rating']."-301 AND rating <= ".$user['rating']."+301) ". 
				"AND (gamesCount >= ".$user['gamesCount']."-26 AND gamesCount <= ".$user['gamesCount']."+26) ".
				"AND users.id NOT IN (SELECT opponentId FROM games WHERE userId = ".$userId.")",
				array("type"=>"LEFT", "table"=>"games", "on"=>"users.id = games.opponentId")
			)->fetch_all();
			file_put_contents('errors.log', "opponents: ".var_export($opponents, true).PHP_EOL,FILE_APPEND);
			$key = rand(0, count($opponents)-1);

			if(!empty($opponents[$key])) {
				$gameData = array(
					"opponentName" => $opponents[$key][1],
					"opponentImage" => $opponents[$key][2],
					"opponentId" => $opponents[$key][0],
					"userId" => $userId
				);
				$result = $db->query("INSERT INTO games(userId, opponentId, expiredDate) VALUES('".$userId."', '".$gameData["opponentId"]."', DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL 1 DAY))");
				$gameData['id'] = $db->last_id();
				$result = $db->query("INSERT INTO games(userId, opponentId, expiredDate) VALUES('".$gameData["opponentId"]."', '".$userId."', DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL 1 DAY))");
				file_put_contents('errors.log', "result: ".var_export($gameData, true).PHP_EOL,FILE_APPEND);
				print json_encode($gameData); 
			} else {
				file_put_contents('errors.log', "result: 0".PHP_EOL,FILE_APPEND);
				print json_encode($gameData);
			}
		} else {
			file_put_contents('errors.log', "result: 0".PHP_EOL,FILE_APPEND);
			print json_encode($gameData);
		}		
	}
?>
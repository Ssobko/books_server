<?php
	header("Content-type: application/json; charset=utf-8");

	file_put_contents('errors.log', "start SELECT ALL ".date("d.m.Y H:i:s").PHP_EOL,FILE_APPEND);
	if(isset($_POST)) {
		file_put_contents('errors.log', "request: ".var_export($_POST,true).PHP_EOL,FILE_APPEND);
		require '../db_connect.php';
		$db = new DB_CONNECT();	
		
		if(isset($_POST['userId'])) {
			$games = array();
			$userId = intval($_POST['userId']);
			settype($userId, "int");
			$result = $db->query("SELECT * FROM games WHERE userId = $userId  AND (status != 5 || status != 0) ORDER BY status");
			$result = mysqli_fetch_all($result,MYSQLI_ASSOC);
			file_put_contents('errors.log', "game data: ".var_export($result, true).PHP_EOL,FILE_APPEND);
			foreach($result as $data) {
				file_put_contents('errors.log', "game data: ".var_export($data, true).PHP_EOL,FILE_APPEND);
				$resultOpponent = $db->selectUserByID($data['opponentId']);
				$data['opponentName'] = ($resultOpponent['nickname'] != "") ? $resultOpponent['nickname'] : "Noname opponent";
				$games['games'][] = $data;
			}
			file_put_contents('errors.log', "games data: ".var_export($games, true).PHP_EOL,FILE_APPEND);

			if(!empty($games)) {
				file_put_contents('errors.log', "result: ".var_export($games, true).PHP_EOL,FILE_APPEND);
				print json_encode($games);
			} else {
				file_put_contents('errors.log', "result: 0".PHP_EOL,FILE_APPEND);
				print json_encode(array());
			}
		}
	} else {
		file_put_contents('errors.log', "result: 0".PHP_EOL,FILE_APPEND);
		print json_encode(array());
	}	
?>
<?php
	header("Content-type: application/json; charset=utf-8");

	error_log("start Select\n", 3, 'errors.log');
	if(isset($_POST)) {
		file_put_contents('errors.log', "request: ".var_export($_POST,true).PHP_EOL,FILE_APPEND);
		require '../db_connect.php';
		$db = new DB_CONNECT();

		if(isset($_POST['id'])) {
			error_log('id: '.$_POST['id']."\n", 3, 'errors.log');
			$game = $db->selectGameByID($_POST['id']);
			
			unset($game['expiredDate']);

			file_put_contents('errors.log', "result: ".var_export($game, true).PHP_EOL,FILE_APPEND);
			print json_encode($game);
		} else {
			file_put_contents('errors.log', "result: 0".PHP_EOL,FILE_APPEND);
			print json_encode(array("statusCode" => 0));
		}
	}
?>
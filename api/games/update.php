<?php
	header("Content-type: application/json; charset=utf-8");

	define('STATUS_OPPONENTS_TURN', 2);
	define('STATUS_PLAYERS_TURN', 3);
	define('STATUS_GAME_FINISHED', 4);

	error_log("start Update\n", 3, 'errors.log');
	if(isset($_POST)) {
		file_put_contents('errors.log', "request: ".var_export($_POST,true).PHP_EOL,FILE_APPEND);
		require '../db_connect.php';
		$db = new DB_CONNECT();

		if(isset($_POST['data']) && $_POST['data'] != "") {
			$gameData = json_decode($_POST['data'], true);
			file_put_contents('errors.log', "game data: ".var_export($gameData, true).PHP_EOL,FILE_APPEND);

			$dbGameData = $db->selectGameByID($gameData['id']);
			$gameData['userPoints'] += $dbGameData['userPoints'];
			$gameData['userRounds'] += $dbGameData['userRounds'];
			file_put_contents('errors.log', "existed game data: ".var_export($dbGameData, true).PHP_EOL,FILE_APPEND);

			if($gameData['id']) {
				$opponentGameData = $db->select("*", "games", "userId = ".$dbGameData['opponentId']." AND opponentId = ".$dbGameData['userId'])->fetch_assoc();
				file_put_contents('errors.log', "opponent game data: ".var_export($opponentGameData, true).PHP_EOL,FILE_APPEND);

				if(isset($gameData['status']) && $gameData['status'] != 0) {
					$userStatus = $opponentStatus = $gameData['status'];
					if($gameData['status'] == STATUS_GAME_FINISHED) {
						$opponent = $db->selectUserByInternalID($dbGameData['opponentId']);
						$user = $db->selectUserByInternalID($dbGameData['userId']);
						$ratingDiff = ((abs($user['rating'] - $opponent['rating']) / 10) > 10) ? abs($user['rating'] - $opponent['rating']) / 10 : 10;

						if($gameData['userPoints'] > $dbGameData['opponentPoints']) { //user victory
							$maxRating = ($user['rating'] + $ratingDiff > $user['maxRating']) ? $user['rating'] + $ratingDiff : $user['maxRating'];
							$maxSeasonRating = ($user['rating'] + $ratingDiff > $user['maxSeasonRating']) ? $user['rating'] + $ratingDiff : $user['maxSeasonRating'];
							$db->update(
								"users", 
								"rating = ".($user['rating'] + $ratingDiff).", maxRating = $maxRating, maxSeasonRating = $maxSeasonRating, winsCount = ".(++$user['winsCount']).
									", gamesCount = ".(++$user['gamesCount']).", gold = ".($user['gold']+rand(0, 6)),
								"id = ".$user['id']								
							);
							$db->update(
								"users", 
								"rating = ".($opponent['rating'] - $ratingDiff).", gamesCount = ".(++$opponent['gamesCount']),
								"id = ".$opponent['id']								
							);
						} else if($gameData['userPoints'] < $dbGameData['opponentPoints']) { //opponent victory
							$maxRating = ($opponent['rating'] + $ratingDiff > $opponent['maxRating']) ? $opponent['rating'] + $ratingDiff : $opponent['maxRating'];
							$maxSeasonRating = ($opponent['rating'] + $ratingDiff > $opponent['maxSeasonRating']) ? $opponent['rating'] + $ratingDiff : $opponent['maxSeasonRating'];
							$db->update(
								"users", 
								"rating = ".($opponent['rating'] + $ratingDiff).", maxRating = $maxRating, maxSeasonRating = $maxSeasonRating, winsCount = ".(++$opponent['winsCount']).
									", gamesCount = ".(++$opponent['gamesCount']).", gold = ".($opponent['gold']+rand(0, 6)),
								"id = ".$opponent['id']								
							);
							$db->update(
								"users", 
								"rating = ".($user['rating'] - $ratingDiff).", gamesCount = ".(++$user['gamesCount']),
								"id = ".$user['id']								
							);
						} else { //draw
							$ratingDiff = 5;
							$maxRating = ($user['rating'] + $ratingDiff > $user['maxRating']) ? $user['rating'] + $ratingDiff : $user['maxRating'];
							$maxSeasonRating = ($user['rating'] + $ratingDiff > $user['maxSeasonRating']) ? $user['rating'] + $ratingDiff : $user['maxSeasonRating'];
							$maxOpponentRating = ($opponent['rating'] + $ratingDiff > $opponent['maxRating']) ? $opponent['rating'] + $ratingDiff : $opponent['maxRating'];
							$maxOpponentSeasonRating = ($opponent['rating'] + $ratingDiff > $opponent['maxSeasonRating']) ? $opponent['rating'] + $ratingDiff : $opponent['maxSeasonRating'];
							$db->update(
								"users", 
								"rating = ".($user['rating'] + $ratingDiff).", maxRating = $maxRating, maxSeasonRating = $maxSeasonRating, gamesCount = ".(++$user['gamesCount']),
								"id = ".$user['id']								
							);
							$db->update(
								"users", 
								"rating = ".($opponent['rating'] + $ratingDiff).", maxRating = $maxOpponentRating, maxSeasonRating = $maxOpponentSeasonRating, gamesCount = ".(++$opponent['gamesCount']),
								"id = ".$opponent['id']								
							);
						}
					}
				} else {
					$userStatus = STATUS_OPPONENTS_TURN;
					$opponentStatus = STATUS_PLAYERS_TURN;
				}

				$result = $db->query("UPDATE games SET userPoints = ".$gameData['userPoints'].", userRounds = ".$gameData['userRounds'].", status = $userStatus, expiredDate = DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL 1 DAY) WHERE id = ".$gameData['id']);
				$opResult = $db->query("UPDATE games SET opponentPoints = ".$gameData['userPoints'].", opponentRounds = ".$gameData['userRounds'].", status = $opponentStatus, expiredDate = DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL 1 DAY) WHERE id = ".$opponentGameData['id']);

				if($result && $opResult) {
					file_put_contents('errors.log', "result: 1".PHP_EOL,FILE_APPEND);
					print json_encode(array("statusCode" => 1)); 
				} else {
					file_put_contents('errors.log', "result: 0".PHP_EOL,FILE_APPEND);
					print json_encode(array("statusCode" => 0));
				}
			} 	
		} else {
			file_put_contents('errors.log', "result: 0".PHP_EOL,FILE_APPEND);
			print json_encode(array("statusCode" => 0));
		}		
	}
?>
<?php
	header("Content-type: application/json; charset=utf-8");

	file_put_contents('errors.log', "start SELECT ".date("d.m.Y H:i:s").PHP_EOL,FILE_APPEND);
	if(isset($_POST)) {
		file_put_contents('errors.log', "request: ".var_export($_POST,true).PHP_EOL,FILE_APPEND);
		require '../db_connect.php';
		$db = new DB_CONNECT();

		if(isset($_POST['userId']) && isset($_POST['category'])) {
			$shopItems = array();
			$userId = $_POST['userId'];
			$category = $_POST['category'];
			settype($userId, "int");
			settype($category, "int");
			file_put_contents('errors.log', "user id: ".var_export($userId,true).PHP_EOL,FILE_APPEND);
			$shopItemsDB = $db->select("*", "shop_items", "category = $category");
			while($shopItem = $shopItemsDB->fetch_assoc()) {
				$userRecipesByCategoryCount = $db->select("recipes.id", "recipes", "users_recipes.userId = $userId AND recipes.categoryId = ".$shopItem['recipe_category'], array("type" => "RIGHT", "table"=>"users_recipes", "on"=>"recipes.id = users_recipes.recipeId"))->num_rows;
				file_put_contents('errors.log', "recipes by category ".$shopItem['recipe_category']." count: ".var_export($userRecipesByCategoryCount,true).PHP_EOL,FILE_APPEND);
				$shopItem['price'] += $userRecipesByCategoryCount * 2;
				$shopItems['shopItems'][] = $shopItem;
			}

			if(!empty($shopItems)) {
				file_put_contents('errors.log', "result: ".var_export($shopItems, true).PHP_EOL,FILE_APPEND);
				print json_encode($shopItems);
			} else {
				file_put_contents('errors.log', "result: 0".PHP_EOL,FILE_APPEND);
				print json_encode(array("id" => 0));
			}
		} else {
			file_put_contents('errors.log', "result: 0".PHP_EOL,FILE_APPEND);
			print json_encode(array("id" => 0));
		}
	}
?>
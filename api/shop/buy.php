<?php
	header("Content-type: application/json; charset=utf-8");

	file_put_contents('errors.log', "start BUY ".date("d.m.Y H:i:s").PHP_EOL,FILE_APPEND);
	if(isset($_POST)) {
		file_put_contents('errors.log', "request: ".var_export($_POST,true).PHP_EOL,FILE_APPEND);
		require '../db_connect.php';
		$db = new DB_CONNECT();

		if(isset($_POST['userId']) && isset($_POST['category']) && isset($_POST['price']) && ($_POST['shopItemId'])) {
			$userId = $_POST['userId'];
			$price = $_POST['price'];
			$shopItemId = $_POST['shopItemId'];
			settype($userId, "int");
			settype($price, "int");
			settype($shopItemId, "int");
			file_put_contents('errors.log', "user id: ".var_export($userId,true).PHP_EOL,FILE_APPEND);
			file_put_contents('errors.log', "shop item price: ".var_export($price,true).PHP_EOL,FILE_APPEND);
			$user = $db->selectUserByID($userId);
			if($user['gold'] >= $price) {
				$goldDiff = $user['gold'] - $price;
				$category = $_POST['category'];
				settype($category, "int");
				file_put_contents('errors.log', "shop item category: ".var_export($category,true).PHP_EOL,FILE_APPEND);

				$randomNewRecipe = $db->select(
					"recipes.*", 
					"recipes", 
					"NOT users_recipes.userId = $userId OR users_recipes.userId IS NULL AND recipes.categoryId = $category", 
					array("type" => "LEFT", "table"=>"users_recipes", "on"=>"recipes.id = users_recipes.recipeId"), 
					"RAND()",
					"1"
				)->fetch_assoc();
				file_put_contents('errors.log', "random new recipe: ".var_export($randomNewRecipe,true).PHP_EOL,FILE_APPEND);

				if(!empty($randomNewRecipe)) {
					$responceGold = $db->update("users", "gold = $goldDiff", "id = $userId");
					$responceRecipe = $db->insert("users_recipes", array("userId" => $userId, "recipeId" => $randomNewRecipe['id'], "date" => "NOW()"));
					$responceOrder = $db->insert(
						"orders", 
						array(
							"userId" => $userId,
							"shopItemId" => $shopItemId,
							"date" => "NOW()",
							"price" => $price
						)
					);
					if($responceGold && $responceRecipe && $responceOrder) {
						$randomNewRecipe['status'] = 0;
						file_put_contents('errors.log', "result: ".var_export($randomNewRecipe, true).PHP_EOL,FILE_APPEND);
						print json_encode($randomNewRecipe);
					} else {
						file_put_contents('errors.log', "result: some query error".PHP_EOL,FILE_APPEND);
						print json_encode(array("id" => 0, "status" => 3));
					}
				} else {
					file_put_contents('errors.log', "result: error - no new recipes found".PHP_EOL,FILE_APPEND);
					print json_encode(array("id" => 0, "status" => 1));
				}
			} else {
				file_put_contents('errors.log', "result: error - not enough gold".PHP_EOL,FILE_APPEND);
				print json_encode(array("id" => 0, "status" => 2));
			}
		} else {
			file_put_contents('errors.log', "result: no POST found".PHP_EOL,FILE_APPEND);
			print json_encode(array("id" => 0, "status" => 3));
		}
	}
?>
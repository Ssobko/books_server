<?php
	header("Content-type: application/json; charset=utf-8");

	file_put_contents('errors.log', "start INSERT ".date("d.m.Y H:i:s").PHP_EOL,FILE_APPEND);
	if(isset($_POST)) {
		file_put_contents('errors.log', "request: ".var_export($_POST,true).PHP_EOL,FILE_APPEND);
		require '../db_connect.php';
		$db = new DB_CONNECT();

		if(isset($_POST['internalId'])) {
			$internalId = $_POST['internalId'];
			settype($internalId, 'string');
			$userDB = $db->selectUserByInternalID($internalId); 
			if(empty($userDB)) {
				$user = array();
				if(isset($_POST['token'])) {
					$requestParams = array(
						'user_id' => $internalId,
						'fields' => 'deactivated',
						'v' => '5.130',
						'access_token' => $_POST['token']
					);
					$getParams = http_build_query($requestParams);
					$VKApiResult = json_decode(file_get_contents('https://api.vk.com/method/users.get?'. $getParams));
					file_put_contents('errors.log', "VK API query result: ".var_export($VKApiResult, true).PHP_EOL,FILE_APPEND);
					if(!$VKApiResult->response[0]->deactivated) {
						$user = array(
							"internalId" => $VKApiResult->response[0]->id,
							"nickname" => $VKApiResult->response[0]->first_name." ".$VKApiResult->response[0]->last_name,
							"email" => "",
							"source" => "VK"
						);
					}
				} else {
					$nickname = $_POST['nickname'];
					$email = $_POST['email'];
					settype($nickname, 'string');
					settype($email, 'string');
					$user = array(
						'internalId' => $internalId,
						'nickname' => $nickname,
						'email' => $email,
						'source' => 'Google'
					);
				}
				if(!empty($user)) {
					$result = $db->insert("users", $user);
					file_put_contents('errors.log', "query result: ".var_export($result, true).PHP_EOL,FILE_APPEND); 
					$userDB = $db->selectUserByInternalID($user['internalId']);
					file_put_contents('errors.log', "new userDB query result: ".var_export($userDB['id'], true).PHP_EOL,FILE_APPEND); 
					print json_encode(array("id" => $userDB['id']));
				} else {
					file_put_contents('errors.log', "user array is empty!".PHP_EOL,FILE_APPEND);
					print json_encode(array("id" => 0));
				}
			} else {
				file_put_contents('errors.log', "userDB query result: ".var_export($userDB['id'], true).PHP_EOL,FILE_APPEND); 
				print json_encode(array("id" => $userDB['id']));
			}
		} else {
			file_put_contents('errors.log', "result: 0\n".PHP_EOL,FILE_APPEND);
			print json_encode(array("id" => 0));
		}		
	}
?>
<?php
	header("Content-type: application/json; charset=utf-8");

	error_log("start delete\n", 3, 'errors.log');
	if(isset($_POST)) {
		file_put_contents('errors.log', "request: ".var_export($_POST,true).PHP_EOL,FILE_APPEND);
		require '../db_connect.php';
		$db = new DB_CONNECT();

		if(isset($_POST['googleId'])) {
			$googleId = $_POST['googleId'];
			$result = $db->query("DELETE FROM users WHERE googleId = $googleId");

			if($result) {
				file_put_contents('errors.log', "result: 1".PHP_EOL,FILE_APPEND);
				print json_encode(array("statusCode" => 1)); 
			} else {
				file_put_contents('errors.log', "result: 0".PHP_EOL,FILE_APPEND);
				print json_encode(array("statusCode" => 0));
			}
		} else {
			file_put_contents('errors.log', "result: 0".PHP_EOL,FILE_APPEND);
			print json_encode(array("statusCode" => 0));
		}		
	}
?>
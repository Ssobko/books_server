<?php
	header("Content-type: application/json; charset=utf-8");

	file_put_contents('errors.log', "start SELECT ".date("d.m.Y H:i:s").PHP_EOL,FILE_APPEND);
	if(isset($_POST)) {
		file_put_contents('errors.log', "request: ".var_export($_POST,true).PHP_EOL,FILE_APPEND);
		require '../db_connect.php';
		$db = new DB_CONNECT();

		if(isset($_POST['id'])) {
			$user = array();
			$id = $_POST['id'];
			settype($id, "int");
			file_put_contents('errors.log', "user id: ".var_export($id,true).PHP_EOL,FILE_APPEND);
			$user = $db->selectUserByID($id);

			if(!empty($user)) {
				file_put_contents('errors.log', "result: ".var_export($user, true).PHP_EOL,FILE_APPEND);
				print json_encode($user);
			} else {
				file_put_contents('errors.log', "result: 0".PHP_EOL,FILE_APPEND);
				print json_encode(array("id" => 0));
			}
		} else {
			file_put_contents('errors.log', "result: 0".PHP_EOL,FILE_APPEND);
			print json_encode(array("id" => 0));
		}
	}
?>
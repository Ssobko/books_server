<?php
	header("Content-type: application/json; charset=utf-8");

	error_log("start\n", 3, 'errors.log');
	file_put_contents('errors.log', "request: ".var_export($_POST,true).PHP_EOL,FILE_APPEND);
	require '../db_connect.php';
	$db = new DB_CONNECT();		
	$result = $db->query("SELECT * FROM users");
	while($data = $result->fetch_assoc()) {
		$users[] = $data;
	}

	if(!empty($users)) {
		file_put_contents('errors.log', "result: ".var_export($users, true).PHP_EOL,FILE_APPEND);
		print json_encode(array("users" => $users));
	} else {
		file_put_contents('errors.log', "result: 0".PHP_EOL,FILE_APPEND);
		print json_encode(array("statusCode" => 0));
	}	
?>
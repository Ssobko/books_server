<?php
	header("Content-type: application/json; charset=utf-8");

	file_put_contents('errors.log', "\nstart SELECT ".date("d.m.Y H:i:s").PHP_EOL,FILE_APPEND);
	if(isset($_POST)) {
		file_put_contents('errors.log', "request: ".var_export($_POST,true).PHP_EOL,FILE_APPEND);
		require '../db_connect.php';
		$db = new DB_CONNECT();

		if(isset($_POST['id'])) {
			error_log('id: '.$_POST['id']."\n", 3, 'errors.log');
			$id = $_POST['id'];
			settype($id, "int");
			$result = $db->selectRecipeByID($id);
			
			file_put_contents('errors.log', "result: ".var_export($result, true).PHP_EOL,FILE_APPEND);
			print json_encode($result);
		} else if(isset($_POST['userId'])) {
			error_log('userId: '.$_POST['userId']."\n", 3, 'errors.log');
			$userId = $_POST['userId'];
			settype($userId, "int");
			$recipesIds = $db->select("recipeId", "users_recipes", "userId = ".$userId." ORDER BY date");
			while($recipe = $recipesIds->fetch_assoc()) {
				$result[] = $db->selectRecipeByID($recipe['recipeId']);
			}
			if(!empty($result)) {
				file_put_contents('errors.log', "result: ".var_export($result, true).PHP_EOL,FILE_APPEND);
				print json_encode(array("recipes" => $result));
			} else {
				file_put_contents('errors.log', "result: 0".PHP_EOL,FILE_APPEND);
				print json_encode(array("statusCode" => 0));
			}
		} else {
			file_put_contents('errors.log', "result: 0".PHP_EOL,FILE_APPEND);
			print json_encode(array("statusCode" => 0));
		}
	}
?>
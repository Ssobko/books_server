<?php
	header("Content-type: application/json; charset=utf-8");

	file_put_contents('errors.log', "\nstart SELECT CHAT ".date("d.m.Y H:i:s").PHP_EOL,FILE_APPEND);
	if(isset($_REQUEST)) {
		file_put_contents('errors.log', "request: ".var_export($_REQUEST,true).PHP_EOL,FILE_APPEND);
		require '../db_connect.php';
		$db = new DB_CONNECT();

		if(isset($_REQUEST['chatId'])) {
			error_log('chatId: '.$_REQUEST['chatId']."\n", 3, 'errors.log');
			$chatId = $_REQUEST['chatId'];
			settype($chatId, "int");

            $messages = $db->getChatHistory($chatId);
            while($message = $messages->fetch_assoc()) {
                $message['author'] = $db->selectUserByID($message['authorId'])['nickname'];
                $formattedDateTime = new DateTime($message['postedDateTime']);
                $message['postedDateTime'] = $formattedDateTime->format("H:i d.m.Y");
                $result[] = $message;
            }

			file_put_contents('errors.log', "result: ".var_export($result, true).PHP_EOL,FILE_APPEND);
			print json_encode(array("chatMessages" => $result));
		} else if(isset($_REQUEST['userId']) && isset($_REQUEST['mateId'])) {
			error_log('userId: '.$_REQUEST['userId']."\n", 3, 'errors.log');
            error_log('mateId: '.$_REQUEST['mateId']."\n", 3, 'errors.log');
			$userId = $_REQUEST['userId'];
            $mateId = $_REQUEST['mateId'];
			settype($userId, "int");
            settype($mateId, "int");
			$chat = $db->getChatByUserIDs($userId, $mateId);

            if($chat) {
                $messages = $db->getChatHistory($chat);
                while($message = $messages->fetch_assoc()) {
                    $message['author'] = $db->selectUserByID($message['authorId'])['nickname'];
                    $formattedDateTime = new DateTime($message['postedDateTime']);
                    $message['postedDateTime'] = $formattedDateTime->format("H:i d.m.Y");
                    $result[] = $message;
                }
            }
			
			file_put_contents('errors.log', "result: ".var_export($result, true).PHP_EOL,FILE_APPEND);
			print json_encode(array("chatMessages" => $result));
		} else if(isset($_REQUEST['clubId'])) {
			error_log('clubId: '.$_REQUEST['clubId']."\n", 3, 'errors.log');
			$clubId = $_REQUEST['clubId'];
			settype($clubId, "int");

			$messages = $db->getClubChat($clubId);
			while($message = $messages->fetch_assoc()) {
				$message['author'] = $db->selectUserByID($message['authorId'])['nickname'];
                $formattedDateTime = new DateTime($message['postedDateTime']);
                $message['postedDateTime'] = $formattedDateTime->format("H:i d.m.Y");
                $result[] = $message;
			}
			
			file_put_contents('errors.log', "result: ".var_export($result, true).PHP_EOL,FILE_APPEND);
			print json_encode(array("chatMessages" => $result));
		} else {
			file_put_contents('errors.log', "result: 0".PHP_EOL,FILE_APPEND);
			print json_encode(array("chatMessages" => array()));
		}
	}
?>
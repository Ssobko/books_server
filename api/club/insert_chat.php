<?php
	header("Content-type: application/json; charset=utf-8");

	file_put_contents('errors.log', "\nstart INSERT CHAT MESSAGE ".date("d.m.Y H:i:s").PHP_EOL,FILE_APPEND);
	if(isset($_POST)) {
		file_put_contents('errors.log', "request: ".var_export($_POST,true).PHP_EOL,FILE_APPEND);
		require '../db_connect.php';
		$db = new DB_CONNECT();

		if(isset($_POST['chatId']) && isset($_POST['userId'])) {
			error_log('userId: '.$_POST['userId']."\n", 3, 'errors.log');
            error_log('chatId: '.$_POST['chatId']."\n", 3, 'errors.log');
			$userId = $_POST['userId'];
            $chatId = $_POST['chatId'];
            $message = htmlspecialchars($_POST['message']);
			settype($userId, "int");
            settype($chatId, "int");

            if($db->insertMessage($chatId, $userId, $message)) {
                $messages = $db->getChatHistory($chatId);
                while($message = $messages->fetch_assoc()) {
                    $message['author'] = $db->selectUserByID($message['authorId'])['nickname'];
                    $formattedDateTime = new DateTime($message['postedDateTime']);
                    $message['postedDateTime'] = $formattedDateTime->format("H:i d.m.Y");
                    $result[] = $message;
                }
            }
			
			file_put_contents('errors.log', "result: ".var_export($result, true).PHP_EOL,FILE_APPEND);
			print json_encode(array("chatMessages" => $result));
		} else {
			file_put_contents('errors.log', "result: 0".PHP_EOL,FILE_APPEND);
			print json_encode(array("chatMessages" => array()));
		}
	}
?>
<?php

class DB_CONNECT {

    //common
    private $con;

    function __construct() {
        $this->connect();
    }

    function __destruct() {
        $this->close($this->con);
    }

    function connect() {
        require 'db_config.php';

        $this->con = mysqli_connect(DB_SERVER, DB_USER, DB_PASSWORD, DB_DATABASE) or die(mysqli_error());

        return $this->con;
    }

    function close($link) {
        mysqli_close($link);
    }

    function query($query) {
        return mysqli_query($this->con, $query);
    }

    function select($fields, $table, $cond = "", $join = array("type"=>"", "table"=>"", "on"=>""), $order = "", $limit = "") {
        $query = "SELECT ".$fields." FROM ".$table;
        if($join['type'] && $join['table'] && $join['on']) $query .= " ".$join['type']." JOIN ".$join['table']." ON ".$join['on'];
        if($cond) $query .= " WHERE ".$cond;
        if($order) $query .= " ORDER BY ".$order;
        if($limit) $query .= " LIMIT ".$limit;
        // file_put_contents('errors.log', "query: ".var_export($query,true).PHP_EOL,FILE_APPEND);
        return $this->query($query);
    }

    function insert($table, $values_array) {
        $fields = "";
        $values = "";
        $i = 1;
        foreach($values_array as $key => $value) {
            $fields .= $key;
            if(strripos($value, "()")) {
                $values .= $value;
            } else {
                $values .= "'".$value."'";
            }
            if($i != count($values_array)) {
                $fields .=", ";
                $values .=", ";
            }
            $i++;
        }
        $query = "INSERT INTO ".$table."(".$fields.") VALUES (".$values.")";
        return $this->query($query);
    }

    function last_id() {
        return $this->insert_id;
    }

    function update($table, $values, $cond) {
        $query = "UPDATE ".$table." SET ".$values." WHERE ".$cond;
        return $this->query($query);
    }

    //Logging

    private function logtoDB($table, $row_id, $column_name, $value_old, $value_new) {
        $query = "UPDATE ".$table." SET ".$values." WHERE ".$cond;
        return $this->query($query);
    }

    //Table `users`

    function selectUserByID($id) {
        return $this->select("*", "users", "id = $id")->fetch_assoc();
    }

    function selectUserByInternalID($id) {
        return $this->select("*", "users", "internalId = $id")->fetch_assoc();
    }

    //Table `games`

    function selectGameByID($id) {
        return $this->select("*", "games", "id = $id")->fetch_assoc();
    }

    function selectGamesByUserID($userId) {
        return $this->select("*", "games", "userId = $userId")->fetch_assoc();
    }

    //Table `recipes`

    function selectRecipeByID($id) {
        return $this->select("*", "recipes", "id = $id")->fetch_assoc();
    }

    //Table `shop_items`

    function selectShopItemByID($id) {
        return $this->select("*", "shop_items", "id = $id")->fetch_assoc();
    }

    //Table `clubs`

    function selectClubByUserID($id) {
        $clubId = $this->select("clubId", "clubs_participants", "userId = $id")->fetch_assoc()['clubId'];
        return $this->select("*", "clubs", "id = $clubId")->fetch_assoc();
    }

    // Table chat

    function getChatByUserIDs($userId, $mateId) {
        $result = $this->select("id", "chats", "(userId = $userId AND mateId = $mateId) OR (userId = $mateId AND mateId = $userId)")->fetch_object();
        if(empty($result)) return $this->createNewDirectChat($userId, $mateId);
        return $result->id;
    }

    function createNewDirectChat($userId, $mateId) {
        if($this->insert("chats", array("userId" => $userId, "mateId" => $mateId))) return $this->insert_id;
        return false;
    }

    function createNewClubChat($clubId) {
        if($this->insert("chats", array("clubId" => $clubId))) return $this->insert_id;
        return false;
    }

    function getChatHistory($chatId) {
        return $this->select("*", "chat_messages", "chatId = $chatId");
    }

    function getClubChat($clubId) {
        $chatId = $this->select("id", "chats", "clubId = $clubId")->fetch_object();
        if(!$chatId->id) return createNewClubChat($clubId);
        return $this->select("*", "chat_messages", "chatId = $chatId->id");
    }

    function insertMessage($chatId, $authorId, $message) {
        return $this->insert("chat_messages", array("authorId" => $authorId, "chatId" => $chatId, "message" => $message));
    }

    //Table `reports`

}

?>